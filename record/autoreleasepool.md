

# 预编译



```
@autoreleasepool { ... }

预编译后，会生成
```

```
/* @autoreleasepool */ { __AtAutoreleasePool __autoreleasepool; 

	... do something ....
}


struct __AtAutoreleasePool {
  __AtAutoreleasePool() {atautoreleasepoolobj = objc_autoreleasePoolPush();}
  ~__AtAutoreleasePool() {objc_autoreleasePoolPop(atautoreleasepoolobj);}
  void * atautoreleasepoolobj;
};

void *
objc_autoreleasePoolPush(void)
{
    return AutoreleasePoolPage::push();
}

NEVER_INLINE
void
objc_autoreleasePoolPop(void *ctxt)
{
    AutoreleasePoolPage::pop(ctxt);
}

```
*	可以明显看出，@autoreleasepool 最后会通过  AutoreleasePoolPage 来管理

# AutoreleasePoolPage

```
struct magic_t {
	static const uint32_t M0 = 0xA1A1A1A1;
#   define M1 "AUTORELEASE!"
	static const size_t M1_len = 12;
	uint32_t m[4];			// 4 byte * 4 = 16 字节

	...
}

struct AutoreleasePoolPageData			// 总计 AutoreleasePoolPageData 会暂用 56 字节的空间
{
	magic_t const magic;				// 16
	__unsafe_unretained id *next;		// 8	
	pthread_t const thread;				// 8
	AutoreleasePoolPage * const parent;	// 8
	AutoreleasePoolPage *child;			// 8
	uint32_t const depth;				// 4
	uint32_t hiwat;						// 4
	...
}

class AutoreleasePoolPage : private AutoreleasePoolPageData { ... 


    static inline id autorelease(id obj)
    {
        ASSERT(obj);
        ASSERT(!obj->isTaggedPointer());
        id *dest __unused = autoreleaseFast(obj);
        ASSERT(!dest  ||  dest == EMPTY_POOL_PLACEHOLDER  ||  *dest == obj);
        return obj;
    }

    static inline id *autoreleaseFast(id obj)
    {
        AutoreleasePoolPage *page = hotPage();
        if (page && !page->full()) {
            return page->add(obj);
        } else if (page) {
            return autoreleaseFullPage(obj, page);
        } else {
            return autoreleaseNoPage(obj);
        }
    }

	id *add(id obj)
    {
        ASSERT(!full());
        unprotect();
        id *ret = next;  // faster than `return next-1` because of aliasing
        *next++ = obj;
        protect();
        return ret;
    }

        static __attribute__((noinline))
    id *autoreleaseFullPage(id obj, AutoreleasePoolPage *page)
    {
        // The hot page is full. 
        // Step to the next non-full page, adding a new page if necessary.
        // Then add the object to that page.
        ASSERT(page == hotPage());
        ASSERT(page->full()  ||  DebugPoolAllocation);

        do {
            if (page->child) page = page->child;
            else page = new AutoreleasePoolPage(page);
        } while (page->full());

        setHotPage(page);
        return page->add(obj);
    }
}

```

AutoreleasePoolPage 是通过next 单向链表 来管理变量的； 通过 parent/child 双向链表来实现 分页管理，当一页放不下变量时会开启新的一页

# POOL_BOUNDARY 游标

POOL_BOUNDARY 哨兵对象，标志着一个自动释放池的边界，在创建 AutoreleasePoolPage时会第一个被 push 到自动释放池的栈顶

```
#   define POOL_BOUNDARY nil

    static inline void *push() 
    {
        id *dest;
        if (slowpath(DebugPoolAllocation)) {
            // Each autorelease pool starts on a new pool page.
            dest = autoreleaseNewPage(POOL_BOUNDARY);
        } else {
            dest = autoreleaseFast(POOL_BOUNDARY);
        }
        ASSERT(dest == EMPTY_POOL_PLACEHOLDER || *dest == POOL_BOUNDARY);
        return dest;
    }
```

# 一个 Page 能存放多少对象？

```
#define PAGE_MAX_SHIFT          14
#define PAGE_MAX_SIZE           (1 << PAGE_MAX_SHIFT)
#define PAGE_MAX_MASK           (PAGE_MAX_SIZE-1)

#define PAGE_MIN_SHIFT          12
#define PAGE_MIN_SIZE           (1 << PAGE_MIN_SHIFT)		// 4096 字节
#define PAGE_MIN_MASK           (PAGE_MIN_SIZE-1)

class AutoreleasePoolPage : private AutoreleasePoolPageData
{
	friend struct thread_data_t;

public:
	static size_t const SIZE =
#if PROTECT_AUTORELEASEPOOL
		PAGE_MAX_SIZE;  // must be multiple of vm page size
#else
		PAGE_MIN_SIZE;  // size and alignment, power of 2
#endif
    
private:
	static pthread_key_t const key = AUTORELEASE_POOL_KEY;
	static uint8_t const SCRIBBLE = 0xA3;  // 0xA3A3A3A3 after releasing
	static size_t const COUNT = SIZE / sizeof(id);
	.....
}

```
* 一个 AutoreleasePoolPage = 4096 字节大小空间，需要扣除 自身大小 56字节，如果是首页需要扣除 哨兵对象（POOL_BOUNDARY）8个字节，即 （4096 - 56 -8）/ 8 = 504 个对象

也可以通过实际例子验证
```
@autoreleasepool {
		for (int i = 0; i < 505; i++) {
            NSObject *p = [[NSObject alloc] autorelease];
        }
        _objc_autoreleasePoolPrint();
}

objc[7778]: ##############
objc[7778]: AUTORELEASE POOLS for thread 0x1000dedc0
objc[7778]: 506 releases pending.
objc[7778]: [0x10100b000]  ................  PAGE (full)  (cold)
objc[7778]: [0x10100b038]  ################  POOL 0x10100b038
objc[7778]: [0x10100b040]       0x100734d70  NSObject
objc[7778]: [0x10100b048]       0x100734de0  NSObject
objc[7778]: [0x10100b050]       0x1007345f0  NSObject
.....
objc[7778]: [0x10100bfe8]       0x100737f20  NSObject
objc[7778]: [0x10100bff0]       0x100737f30  NSObject
objc[7778]: [0x10100bff8]       0x100737f40  NSObject
objc[7778]: [0x10100d000]  ................  PAGE  (hot) 
objc[7778]: [0x10100d038]       0x100737f50  NSObject
objc[7778]: ##############


(lldb) p 0x10100b038-0x10100b000
(long) $1 = 56		// AutoreleasePoolPage 对象空间
(lldb) p 0x10100b040-0x10100b038	
(long) $0 = 8		// POOL_BOUNDARY 占用的 空间

```







