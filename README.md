

### 引用 [objc4源码编译与调试](https://blog.csdn.net/ShadowOfMaster/article/details/112364087)

### 执行方式
1、修改 Deployment Target 为 10.14
2、执行objcDemo

### 学习记录
1、[isa 理解](./record/isa.md)
2、[autoreleasepool 理解](./record/autoreleasepool.md)

### 参考
1、[iOS 底层原理之 类的原理分析](https://blog.csdn.net/Mr_yu__/article/details/117989229)